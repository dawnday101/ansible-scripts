!#/usr/bin/env bash

cd /home/ec2-user/ansible-scripts

# full update on machine
sudo yum update -y ; \
sudo yum install -y \
    git \
    epel-release \
    ansible \
    ansible-galaxy \
    curl

chmod +x install-ansible.sh
sudo echo "ansible ALL=(ALL)     NOPASSWD: ALL" >> /etc/sudoers
sudo echo "<name of hosts>" >> /etc/ansible/hosts
sudo useradd ansible






